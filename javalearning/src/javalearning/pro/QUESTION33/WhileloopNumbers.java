package javalearning.pro.QUESTION33;

public class WhileloopNumbers {
	public static void main(String[]args) {
		int num1 = 10;
		int num2 = 1;
		
		while (num2 <= num2/2 ) {
			int even = num2 * 2;
			String value = String.format("%03d",even);
			System.out.println(value+" = ");
			num2 = num2 + 1;
			
		}
				
	}

}
