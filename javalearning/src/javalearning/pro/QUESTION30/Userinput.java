package javalearning.pro.QUESTION30;

import java.util.Scanner;

public class Userinput {
	public static Scanner scan = new Scanner(System.in);
	
	public static int readNumbers(String msg) {
		System.out.println(msg + ": ");
		return scan.nextInt();
		}
	public static double readDecimals(String msg) {
		System.out.println(msg + ": ");
		return scan.nextInt();
	}
	public static String readText(String msg) {
			System.out.println(msg + ": ");
			return scan.nextLine();
			}

}
