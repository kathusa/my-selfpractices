package javalearning.pro.QUESTION23;

public class Stringmethod {
	public static void main(String[]args) {
		String WORD = " school.of.Progressive.rock";
		System.out.println(WORD.length());
		System.out.println(WORD.substring(22));
		System.out.println(WORD.substring(22,24));
		System.out.println(WORD.indexOf("oo"));
		System.out.println(WORD.toUpperCase());
		System.out.println(WORD.lastIndexOf("o"));
		System.out.println(WORD.indexOf("ok"));
	}
}

//*
27
.rock
.r
4
 SCHOOL.OF.PROGRESSIVE.ROCK
24
-1
\\*