package javalearning.pro.QUESTION44;

public class DecimalToBinaryWithBuilder {
	public static void main (String[]args) {
		int num = 97;
	     int binary[] = new int[40];
	     int index = 0;
	     while(num > 0){
	       binary[index++] = num%2;
	       num = num/2;
	     }
	     for(int i = index-1;i >= 0;i--){
	       System.out.print(binary[i] );
	       
	     }
	  }
	 
	  
}




/*OUT PUT
 * 1100001
 * 
 */
 
