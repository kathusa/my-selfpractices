package javalearning.pro.QUESTION34;

public class Dowhileloop {
	public static void main(String[]args) {
		int num1 = 10;
		int num2 = 1;
		do {
		int odd = ((num2*2)-1);
		int even = num2 * 2;
		
		String oddvalue = String.format("%03d",odd);
		String evenvalue = String.format("%03d",even);
		System.out.println(oddvalue+ " .0 "+","+evenvalue+".0");
		num2 = num2 + 1;
		}while (num2 <= num1/2 ) ;
			
			
		}
				
	}

\\*
001 .0 ,002.0
003 .0 ,004.0
005 .0 ,006.0
007 .0 ,008.0
009 .0 ,010.0

//*



