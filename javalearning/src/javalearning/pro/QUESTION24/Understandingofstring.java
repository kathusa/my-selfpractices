package javalearning.pro.QUESTION24;

public class Understandingofstring {
	public static void main(String[]args) {
		String w = "Singin'_ in_ the_ rain";
		String pat = "in";
		
		
		System.out.println(w.indexOf(pat));
		System.out.println(w.indexOf(pat,3));
		System.out.println(w.indexOf(pat,6));
		System.out.println(w.lastIndexOf("pat"));
		System.out.println(w.length());
		System.out.println(w.toUpperCase());
		System.out.println(w.charAt(0));
	}

}

\\*
1
4
9
-1
22
SINGIN'_ IN_ THE_ RAIN
S
//*