package Bcas.un.Ts;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import junit.framework.Assert;

public class FindAreaTest {

	FindArea FindArea = new FindArea();
	
	
	@Test
	public void circle_Test() {
		double radious = 7;
		double expected = 153.93804002589985;
		double actual = FindArea.circle(radious);
		
		Assert.assertEquals(153.93804002589985 , FindArea.circle(radious));
	}
	
	@Test
	public void circle_Test_withZero() {
		Assert.assertEquals(0, 0);
		
	}
	
	@Test
	public void rectangle_Test() {
		Assert.assertEquals(50.0,FindArea.reactangle(10,5));
	}
	
	@Test
	public void sayHi_Test() {
		assertEquals("HeyKATHUSHA",FindArea.sayHeyKATHUSHA());
	}
	
	
}